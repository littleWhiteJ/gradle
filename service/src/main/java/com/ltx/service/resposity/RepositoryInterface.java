package com.ltx.service.resposity;

import com.ltx.service.pojo.StockDTO;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

import java.util.Map;

public interface RepositoryInterface {
    @FormUrlEncoded
    @POST("131-46")
    @Headers("Content-Type: application/x-www-form-urlencoded")
    Call<StockDTO> getRealTimeStockData(@FieldMap Map<String, String> param);
}
