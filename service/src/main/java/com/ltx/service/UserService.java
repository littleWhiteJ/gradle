package com.ltx.service;

import com.ltx.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserDao userDao;
    public Object getUserInfo() {
        return userDao.getAllUser();
    }
}
