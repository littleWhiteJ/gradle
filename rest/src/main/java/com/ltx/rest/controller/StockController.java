package com.ltx.rest.controller;

import com.ltx.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/stock")
public class StockController {
    @Autowired
    StockService stockService;

    @GetMapping
    public Object getAllStack(@RequestParam("id") String appId,
                              @RequestParam("sign") String sign,
                              @RequestParam("stocks") String stocks,
                              @RequestParam("needIndex") String needIndex) {
        return stockService.getStockInfo(appId,sign,stocks,needIndex);
    }
}
